/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package util;

/**
 * A limited, unfair Semaphore implementation.
 *
 * @author Jonathon Hope
 */
public class UnfairSemaphore implements Semaphore {

    /**
     * The number of resources.
     */
    protected int permits;

    /**
     * Construct a new Semaphore object with the
     * given number of permits.
     *
     * @param permits the number of resources to manage
     *                using this Semaphore.
     */
    public UnfairSemaphore(int permits) {
        this.permits = permits;
    }

    @Override
    public void acquire() throws InterruptedException {
        if (Thread.interrupted()) throw new InterruptedException();
        synchronized(this) {
            try {
                while (permits <= 0)
                    wait();
                --permits;
            }
            catch (InterruptedException ex) {
                notify();
                throw ex;
            }
        }
    }

    @Override
    public synchronized void release() {
        ++permits;
        notify();
    }

    @Override
    public boolean attempt(long milliSeconds) throws InterruptedException {
        if (Thread.interrupted()) throw new InterruptedException();

        synchronized(this) {
            if (permits > 0) {
                --permits;
                return true;
            }
            else if (milliSeconds <= 0)
                return false;
            else {
                try {
                    long startTime = System.currentTimeMillis();
                    long waitTime = milliSeconds;

                    while (true) {
                        wait(waitTime);
                        if (permits > 0) {
                            --permits;
                            return true;
                        }
                        else {
                            waitTime = milliSeconds - (System.currentTimeMillis() - startTime);
                            if (waitTime <= 0)
                                return false;
                        }
                    }
                }
                catch(InterruptedException ex) {
                    notify();
                    throw ex;
                }
            }
        }
    }

}
