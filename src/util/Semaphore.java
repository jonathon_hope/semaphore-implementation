package util;

/**
 * Specifies the interface for a general synchronisation tool for
 * managing shared resources, with the proviso that it is used
 * in the following manner:
 * <pre>
 *     Semaphore s = ...
 *     // ...
 *     try {
 *         s.acquire();
 *
 *         // critical section
 *
 *     } catch (InterruptedException e) {
 *         //...
 *     }
 *     finally {
 *         s.release();
 *     }
 * </pre>
 *
 * @author Jonathon Hope
 */
public interface Semaphore {

    /**
     * Attempts to <em> acquire </em> exclusive access to a
     * resource, but waits indefinitely if another object has
     * already <em> acquired </em>.
     *
     * @throws InterruptedException if the current {@link Thread}
     *                              is interrupted.
     */
    void acquire() throws InterruptedException;

    /**
     * The opposite of {@link #acquire()}. This method releases
     * exclusive access to a resource.
     * <p/>
     * Does not throw {@link java.lang.InterruptedException} so that
     * it may be used in {@code finally} blocks.
     */
    void release();

    /**
     * Wait at most for the duration specified, returning
     * an indication of success or failure.
     *
     * @param milliSeconds the approximate number of milliseconds
     *                     to wait for before cancelling the acquire
     *                     operation. This is not guaranteed, but
     *                     simply provides a guide for this method.
     * @return {@code true} if the current {@link Thread} has acquired
     * the resource.
     * @throws InterruptedException if the current {@link Thread}
     *                              is interrupted.
     */
    boolean attempt(long milliSeconds) throws InterruptedException;

}
