/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package util;


/**
 * Provides a base framework for implementing the {@link util.Semaphore}
 * interface based on queuing.
 *
 * @author Jonathon Hope
 */
public abstract class AbstractQueuedSemaphore extends UnfairSemaphore {

    protected final Queue queue;

    AbstractQueuedSemaphore(Queue queue, int permits) {
        super(permits);
        this.queue = queue;
    }

    /**
     * todo document.
     */
    @Override
    public void acquire() throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        if (precheck())
            return;
        Queue.Node node = new Queue.Node();
        node.doWait(this);
    }

    /**
     * todo document
     */
    @Override
    public boolean attempt(long msecs) throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        if (precheck())
            return true;
        if (msecs <= 0)
            return false;

        Queue.Node node = new Queue.Node();
        return node.doTimedWait(this, msecs);
    }

    /**
     * Check for available permits.
     *
     * @return {@code true} if there are available permits.
     */
    protected synchronized boolean precheck() {
        boolean pass = (permits > 0);
        if (pass)
            --permits;
        return pass;
    }

    /**
     * Similar to {@link #precheck()} but also performs an enqueue
     * of a new node to wait on.
     *
     * @return {@code true} if there are available permits.
     */
    protected synchronized boolean recheck(Queue.Node node) {
        boolean pass = (permits > 0);
        if (pass)
            --permits;
        else
            queue.insert(node);
        return pass;
    }

    /**
     * @return the node that was signalled (ie. the head of the Queue).
     */
    protected synchronized Queue.Node getSignallee() {
        Queue.Node node = queue.poll();
        if (node == null)
            ++permits; // if none, inc permits for new arrivals
        return node;
    }

    /**
     * todo document
     */
    public void release() {
        while (true ) {
            Queue.Node head = getSignallee();
            if (head == null)
                return;  // Simply return as there is no one waiting
            if (head.signal()) // notify if still waiting, else skip
                return;
        }
    }

    /**
     * The internal mechanism for implementing the
     * queued semaphore behavior. To be implemented in
     * subclasses.
     */
    protected static abstract class Queue {

        /**
         * @param node the Node to insert into
         *             this Queue.
         */
        protected abstract void insert(Node node);

        /**
         * @return the node at the head of the Queue
         * or {@code null} if there is no head.
         */
        protected abstract Node poll();

        /**
         * A waiting node.
         */
        protected static class Node {

            /**
             * The wait condition. This will be true, if
             */
            boolean waiting;

            /**
             * The next node in the queue.
             */
            Node next;

            protected Node() {
                this.waiting = true;
                next = null;
            }

            /**
             * If the wait condition is true then we must notify
             * otherwise simply return {@code false}.
             */
            protected synchronized boolean signal() {
                boolean signalled = waiting;
                if (signalled) {
                    waiting = false;
                    notify();
                }
                return signalled;
            }

            /**
             * @param semaphore the wrapping AbstractQueuedSemaphore that
             *                  manages the Queue, or a subclass thereof.
             * @throws InterruptedException if the waiting operation is interrupted.
             */
            protected synchronized void doWait(AbstractQueuedSemaphore semaphore)
                    throws InterruptedException {
                if (!semaphore.recheck(this)) {
                    try {
                        while (waiting)
                            wait();
                    } catch (InterruptedException ex) {
                        if (waiting) {
                            // invalidate the condition for the signaller
                            waiting = false;
                            throw ex;
                        } else {
                            // thread was interrupted after it was notified
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            }

            /**
             * Perform the equivalent of {@link #attempt(long)} but as a
             * queued operation.
             *
             * @param semaphore    the wrapping AbstractQueuedSemaphore that
             *                     manages the Queue, or a subclass thereof.
             * @param milliSeconds the approximate number of milliseconds
             *                     to wait for before cancelling the acquire
             *                     operation. This is not guaranteed, but
             *                     simply provides a guide for this method.
             * @return {@code true} if the resource was acquired and {@code false} otherwise.
             * @throws InterruptedException if the operation is interrupted.
             */
            protected synchronized boolean doTimedWait(AbstractQueuedSemaphore semaphore,
                                                       long milliSeconds)
                    throws InterruptedException {
                if (semaphore.recheck(this) || !waiting)
                    return true;
                else if (milliSeconds <= 0) {
                    waiting = false;
                    return false;
                } else {
                    long waitTime = milliSeconds;
                    long start = System.currentTimeMillis();

                    try {
                        while (true ) {
                            wait(waitTime);
                            if (!waiting)   // definitely signalled
                                return true;
                            else {
                                waitTime = milliSeconds - (System.currentTimeMillis() - start);
                                if (waitTime <= 0) { // timed out
                                    waiting = false;
                                    return false;
                                }
                            }
                        }
                    } catch (InterruptedException ex) {
                        if (waiting) {
                            // invalidate condition for the signaller
                            waiting = false;
                            throw ex; // re-throw
                        } else {
                            // thread was interrupted after it was notified
                            Thread.currentThread().interrupt();
                            return true;
                        }
                    }
                }
            }
        }
    }
}
