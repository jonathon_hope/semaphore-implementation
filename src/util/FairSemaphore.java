/**
 * Copyright(C) 2014 <a href="mailto:Jonathon.a.hope@gmail.com" >Jonathon Hope</a>
 */

package util;

/**
 * A fair semaphore, based on queuing operations.
 *
 * @author Jonathon Hope
 */
public class FairSemaphore extends AbstractQueuedSemaphore {

    public FairSemaphore(int initialPermits) {
        super(new FairQueue(), initialPermits);
    }

    /**
     * This is a linked Queue because I could not be bothered
     * implementing an Array based one.
     * todo reimplement as array.
     */
    protected static class FairQueue extends Queue {
        protected Node head = null;
        protected Node tail = null;

        protected void insert(Node w) {
            if (tail == null)
                head = tail = w;
            else {
                tail.next = w;
                tail = w;
            }
        }

        protected Node poll() {
            if (head == null)
                return null;
            else {
                Node w = head;
                head = w.next;
                if (head == null) tail = null;
                w.next = null;
                return w;
            }
        }
    }
}
