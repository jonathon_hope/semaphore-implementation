# Semaphore implementation

Realistically no one should be writing code like this any more,
in light of the java.util.concurrent package. However it is appropriate
for use in cases where this is not possible such as legacy code or silly university
(or otherwise) assignments that make you learn synchronisation mechanisms using Java.

It has also been designed with the view that other Unfair mechanisms may be used.